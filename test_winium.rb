require 'selenium-webdriver'
require 'rubygems'
require 'win32ole'
require 'test/unit/assertions'

include Test::Unit::Assertions

def find_process(processname)
  status = `tasklist | find "#{processname}"`
  if status.empty?
    status = '0'
  else
    status = '1'
  end
  return status
end

##Start the Winium Driver
shell = WIN32OLE.new('Shell.Application')
#open Winium Desktop Driver - confirm the location
shell.ShellExecute('C:\Winium.Desktop.Driver.exe')

#class which launches the windows app and connects Winium url to Selenium for Automation testing
class WiniumApp
  def launch_Application
    caps = Selenium::WebDriver::Remote::Capabilities.new
    #confirm the Location of application
    caps['app']  =  'C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Sage Payment Solutions\Sage Exchange Desktop.lnk'
    @driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999" , desired_capabilities: caps
  end
end

#
# # #Create new WiniumApp object to lunch application
SEDApp = WiniumApp.new
# #
# # #execute the launchApplication method in class WiniumApp
app = SEDApp.launchApplication
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## Code to right click on SED tray icon and select Settings
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ##use parent window details in variable window1
def get_sed_version (app)
  if find_process("SageExchange.exe") == '1'
    app.find_element(:id, "1502").click
    sed = app.find_element(:name, "Sage Exchange")
    app.action.context_click(sed).perform
    app.find_element(:name, "About Sage Exchange Desktop").click
    window = app.find_element(:id, "frmAbout")
    version = window.find_element(:id,"lblVersion").attribute("Name")
    app.find_element(:name, "About").find_element(:name,"Close").click
    return version
  else
    return "SED is not running, Please start SED"
  end
end

puts get_sed_version (app)
# app.find_element(:id, "1502").click
# sed = app.find_element(:name, "Sage Exchange")
# app.action.context_click(sed).perform
# app.find_element(:name, "Settings").click
#
# window1 = app.find_element(:name, "Sage Exchange - Settings")
# window1.find_element(:name,"EMV Devices").click
# window1.find_element(:name,"Equinox L5300").click
# window1.find_element(:name,"Equinox L5300").click
# sleep 1
# window1.find_element(:name,"Automatically start card swipe").click
# window1.find_element(:name,"Automatically print EMV receipt").click
# sleep 1
# window1.find_element(:name,"OK").click
# app.find_element(:name,"Close").click


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## Code to right click on SED tray icon and get About details
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# ##use parent window details in variable window1
# app.find_element(:id, "1502").click
# sed = app.find_element(:name, "Sage Exchange")
# app.action.context_click(sed).perform
# app.find_element(:name, "About Sage Exchange Desktop").click
#
# window1 = app.find_element(:name, "About")
# assert(window1.find_element(:name,"v2.0.2.16"))
# window1.find_element(:name, "Close").click
#
# ##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
#
# sleep 5

# #Close the application
system("taskkill /IM SpsDevboxCs.exe")

#Close the WiniumDriver
system("taskkill /IM Winium.Desktop.Driver.exe")

#Close the WiniumDriver - still not working
#shell.Quit();