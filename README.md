#SED Automation using Winium Driver

This project automates the test cases for Sage Exchange Desktop application 
on Windows OS, Project uses Ruby and Cucumber along with Winium Driver for 
UI automation

##Pre-requisites

- Ruby should already be installed along with below gems    
- cd to the repository and run `bundle install`, this install all the gems present in GemFile
    
- Copy `Winium.Desktop.Driver.exe` from Winium_Driver folder to `C:\ `
- Copy SPSDevBoxCs_latest_62416 to `C:\SPSDevBoxCs_latest_62416`
    * Get this from shared drive
    
##Folder structure    
- All features files are located are `sps_sed2_automation\features`
- Steps files is at `sps_sed2_automation\features\steps_definitions\sps_sed2_steps.rb`
- Winium_functions file is located at `sps_sed2_automation\features\support\winium_functions.rb`

##Object Identification
- Use Inspect.exe located at `sps_sed2_automation\Inspect_tool\inspect.exe` to identify elements and get the related properties of the object

## Execution

To execute each feature, open cmd terminal as administrator, __The user account should have admin privileges else scripts wont pass__
cd to sps_sed2_automation folder and execute each feature like below example
```
cucumber features\Silent_Installation.feature --format html --out Silent_Installation_Test_Results_report.html --format pretty
```

Test report Silent_Installation_Test_Results_report.html will be generated in `sps_sed2_automation` folder

## Winium documentation

- Winium documentation is available from below links
    *  https://github.com/2gis/Winium.Desktop/wiki
    * https://github.com/2gis/Winium.Desktop/wiki/Finding-Elements
    
- Winium uses Selenium Webdriver internally so we can also refer to selenium documentation
    * https://gist.github.com/kenrett/7553278
    * https://gist.github.com/huangzhichong/3284966
    
     



    
      