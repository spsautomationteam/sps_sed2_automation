@file-validation
@ui
Feature: RBA driver installer
  Testing the Microsoft Visual C++ 2012 in windows 7

  Scenario: Validating RBA Driver
    Given User downloads Device Driver Ingenico iPP320
    When  User validates if the device driver is installed
    Then  User validates if Driver is installed and shows up in Control Panel
    And   User validates the driver file is present at the required location

