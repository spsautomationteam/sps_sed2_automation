## NOTE : this feature has to run via command line as admin

@installations
  @ui
  Feature:  Validating uninstall window confirmation title

  Scenario:  SED 2.0 uninstall validation of window title
    Given  user downloads SED2.0 software from QA url
    And   User validates if manual installer is downloaded
    And  user extracts SPS bootstrapper on local machine
    When User Opens SageExchangeDesktopBootstrapper tool
    And User uninstalls SED if already installed
    And User Opens SageExchangeDesktopBootstrapper tool
    And User Installs SED in local machine
    And User clicks Download and Install button