require_relative '../support/winium_functions.rb'
require 'test/unit/assertions'
require 'watir'
# require 'watir-scroll'
require 'zip'
require "open-uri"
require "win32ole"


include Test::Unit::Assertions



##Adding username to get the current windows username of the machine from
## Ruby Environment variable
$username = ENV['USERNAME']
puts $username

## create sed_installer directories
system "mkdir C:\\Users\\#{$username}\\sed_installers\\prod 2>NUL"
system "mkdir C:\\Users\\#{$username}\\sed_installers\\qa 2>NUL"

##Funtion to unzip a zip file in Ruby
##provide name of zipFile to file and folder to extract to destination
def extract_zip(file, destination)
  FileUtils.mkdir_p(destination)

  Zip::File.open(file) do |zip_file|
    zip_file.each do |f|
      fpath = File.join(destination, f.name)
      zip_file.extract(f, fpath) unless File.exist?(fpath)
    end
  end
end

##Download a file from a URL, provide url and path(provide dir path and name of file)
def download(url, path)
  File.open(path, "w") do |f|
    IO.copy_stream(open(url), f)
  end
end

## Function which reads the contents of the file (filename - complete path of the file)
## into a string and returns the variable 'data' which is a string
def get_file_as_string(filename)
  data = ''
  f = File.open(filename, "r")
  f.each_line do |line|
    data += line
  end
  return data
end

## Function which searches if application 'processname' is running, returns status 0 if not running
## returns status 1 if the process is running
def find_process(processname)
  status = `tasklist | find "#{processname}"`
  if status.empty?
    status = '0'
  else
    status = '1'
  end
  return status
end

## Funtion to get SED Version
def get_sed_version (app)
  if find_process("SageExchange.exe") == '1'
    app.find_element(:id, "1502").click
    sed = app.find_element(:name, "Sage Exchange")
    app.action.context_click(sed).perform
    app.find_element(:name, "About Sage Exchange Desktop").click
    window = app.find_element(:id, "frmAbout")
    version = window.find_element(:id,"lblVersion").attribute("Name")
    app.find_element(:name,"Close").click

    wsh = WIN32OLE.new('Wscript.Shell')
    wsh.AppActivate('About')
    wsh.SendKeys('%{F4}')

    return version
  else
    return "SED is not running, Please start SED"
  end
end


Before do
  begin
    ##close winuim driver if already opened.
    system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
    #Close app if already open
    system("taskkill /IM SpsDevboxCs.exe /f >nul 2>&1")
    #Close bootstrapper if open
    system("taskkill /IM SageExchangeDesktopBootstrapper.exe /f >nul 2>&1")
  rescue => e
    puts "Application not found, continuing with execution"
  end
end




Given(/^User Opens SPSDevBox tool$/) do
  ##Start the Winium Driver
  shell = WIN32OLE.new('Shell.Application')

  #open Winium Desktop Driver - confirm the location
  shell.ShellExecute('C:\Winium.Desktop.Driver.exe')

# #Create new WiniumApp object from Winium_functions.rb file
  SEDApp = WiniumApp.new
#
# #execute the launch_sps_dev_application method in class WiniumApp
  # # and assign value to instance variable @app
  @app = SEDApp.launch_sps_dev_application
end
#Click on Button with name on parent window1, passed the button and window name as arguments
And(/^User clicks (.*) button in window (.*)$/) do |button_name, window_name|
  window = @app.find_element(:class, "Window").find_element(:name, "#{window_name}")
  window.find_element(:class, "Button").find_element(:name, "#{button_name}").click
end

## Provided Menu items as argument and then split them into an array
And(/^User clicks Menu item TEMPLATES - (.*) and clicks GO$/) do |arg1|
  menu_items = arg1.split(" ")
  window = @app.find_element(:class, "Window").find_element(:name, "Sage Exchange")
  #window.find_element(:name, "Maximize").click
  menu = window.find_element(:class,"Menu")
  menu.find_element(:name, "TEMPLATES").click
  menu.find_element(:name, "#{menu_items[0]}").click
  menu.find_element(:name, "#{menu_items[1]}").click
  menu.find_element(:name, "#{menu_items[2]}").click
  window.find_element(:class,"Button").find_element(:name,"GO").click
end

When(/^User clicks GO$/) do
  window = @app.find_element(:class, "Window").find_element(:name, "Sage Exchange")
  window.find_element(:class,"Button").find_element(:name,"GO").click
end

And(/^User provides XML input data for (.*) to SPSDevBox with valid MID$/) do |type|
  window = @app.find_element(:class, "Window").find_element(:name, "Sage Exchange")
  window.find_element(:id, "RawInput").clear
  path = File.expand_path("../../test_data/sed_#{type}_input.xml", __FILE__)
  xml_input = get_file_as_string(path)
  window.find_element(:id, "RawInput").send_keys xml_input
  sleep 10
end

# #Validating if SED Window is open using Assert
# Then(/^SED pop up window should open$/) do
# assert(@app.find_element(:class, "WindowsForms10.Window.8.app.0.34f5582_r9_ad1").find_element(:name,"Sage Exchange - Sale"))
# end

#Validating if SED Window is open using Assert
Then(/^SED pop up window (.*) should open$/) do |window_name|
  assert(@app.find_element(:name,"#{window_name}"))
  @window = @app.find_element(:class, "WindowsForms10.Window.8.app.0.34f5582_r9_ad1").find_element(:name,"#{window_name}")
end

#Select radio button ACH
And(/^User selects Payment type ACH$/) do
@window.find_element(:id,"btnVirtualCheck").click
end

# Code to click on Next button
And(/^User clicks next$/) do
  @window.find_element(:id,"btnNext").click
end

# Code to click on Next button
And(/^User clicks Submit button$/) do
  @window.find_element(:id,"btnSubmit").click
end

#Validating if default value is checking
And(/^By default value of Account type dropdown should be 'Checking'$/) do
  @window.find_element(:id, "ddlAccountType").find_element(:id,"ddlValue").click
  assert(@window.find_element(:id, "ddlAccountType").find_element(:id,"ddlValue").find_element(:name,"Checking"))
  sleep 3
  @window.find_element(:name, 'Close').click
  @window.find_element(:name, 'Close').click
end

#Set Routing number
And(/^User sets (.*) for Routing Number in SED Payment Information$/) do |routing_number|
  @window.find_element(:id, "ffRoutingNumber").find_element(:id,"txtValue").send_keys "#{routing_number}"
end

#Set Account number
And(/^User sets (.*) for Account Number in SED Payment Information$/) do |account_number|
  @window.find_element(:id, "ffAccountNumber").find_element(:id,"txtValue").send_keys "#{account_number}"
end

#Set Account Type
And(/^User selects (.*) for Account Type dropdown in SED Payment Information$/) do |account_type|
  @window.find_element(:id, "ddlAccountType").find_element(:id,"ddlValue").click
  @window.find_element(:id, "ddlAccountType").find_element(:id,"ddlValue").find_element(:name,"#{account_type}").click
end

#Set Class Type
And(/^User selects (.*) for Class dropdown in SED Payment Information$/) do |trans_type|
  @window.find_element(:id, "ddlTranType").find_element(:id,"ddlValue").click
  @window.find_element(:id, "ddlTranType").find_element(:id,"ddlValue").find_element(:name,"#{trans_type}").click
end

#Validate Response Message and Transaction type
Then (/^User should be able to validate the Response Message - (.*) and Transaction Class - (.*)$/) do |response_message, trans_class|
  window = @app.find_element(:class, "Window").find_element(:name, "Sage Exchange")

  #get the text of xml response from SPSDevBox
  response = window.find_element(:id, "RawOutput").text

  #Validate the response message with the xml response text
  expect("#{response}").to include("#{response_message}")

  #Validate the trans class with xml response text
  expect("#{response}").to include("#{trans_class}")
end

Given (/^user downloads SED2.0 software from Production url$/) do
    ##Validate if zip file is already available in Downloads folder, if available delete it
    if File.exist?("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip")
      Cucumber.logger.debug("File exists.")
      File.delete("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip")
    end

    preferences = {
        :download => {
            :prompt_for_download => false,
            :directory_upgrade => true,
            :default_directory => "download_directory"
        }
    }

    ## Open new browser window
    @browser = Watir::Browser.new :chrome, :prefs => preferences, :switches => %w[--safebrowsing-disable-download-protection]
    #@browser.driver.manage.window.maximize
    @browser.goto 'https://www.sageexchange.com/install/#/sedv2x/en'

    ## Click the download link
    download = @browser.links(:text => "Sage Exchange Desktop v2.x installer")[0]
    download.click
    sleep 5

    ## Select the check box to agree and then download
    agree = @browser.input(:type => "checkbox")
    agree.click
    sleep(3)
    @browser.link(:href => "SageExchangeDesktopBootstrapper.zip").click
    @env = 'prod'
end

Given (/^user downloads SED2.0 software from QA url$/) do
  ##Validate if zip file is already available in Downloads folder, if available delete it
    if File.exist?("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip")
      Cucumber.logger.debug("File exists.")
      File.delete("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip")
    end
    ##Validate if zip file is already available in Installers folder, if available delete it
    if File.exist?("C:\\Users\\#{$username}\\sed_installers\\qa\\SageExchangeDesktopBootstrapper.zip")
      Cucumber.logger.debug("File exists.")
      File.delete("C:\\Users\\#{$username}\\sed_installers\\qa\\SageExchangeDesktopBootstrapper.zip")
    end


    preferences = {
        :download => {
            :prompt_for_download => false,
            :directory_upgrade => true,
            :default_directory => "download_directory"
        }
    }

    ## Open new browser window
    @browser = Watir::Browser.new :chrome, :prefs => preferences, :switches => %w[--safebrowsing-disable-download-protection]
    # @browser.driver.manage.window.maximize
    @browser.goto 'https://qa.sageexchange.com/install/#/sedv2x/en'
    ## Click the download link
    download = @browser.links(:text => "Sage Exchange Desktop v2.x installer")[0]
    download.click
    sleep 5

    ## Select the check box to agree and then download
    agree = @browser.input(:type => "checkbox")
    agree.click
    sleep(3)
    @browser.link(:href => "SageExchangeDesktopBootstrapper.zip").click
    sleep 30
  @env = 'qa'
end

Given (/^user downloads SED2.0 Silent Installer from QA url$/) do
  # ##Validate if zip file is already available in Downloads folder, if available delete it
  if File.exist?("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi")
    Cucumber.logger.debug("File exists.")
    File.delete("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi")
  end
  #
  # preferences = {
  #     :download => {
  #         :prompt_for_download => false,
  #         :directory_upgrade => true,
  #         :default_directory => "download_directory"
  #     }
  # }
  #
  # ## Open new browser window
  # @browser = Watir::Browser.new :chrome, :prefs => preferences, :switches => %w[--safebrowsing-disable-download-protection]
  # #@browser.driver.manage.window.maximize
  # @browser.goto 'https://qa.sageexchange.com/install/#/sedv2x/en'
  #
  # ## Click the download link
  # download = @browser.links(:text => "Download Installer")[1]
  # download.click
  # sleep 5
  #
  # ## Select the check box to agree and then download
  # agree = @browser.input(:type => "checkbox")
  # agree.click
  # sleep(3)
  # @browser.link(:href => "SageExchangeDesktopDistributer.msi").click
  @env = 'qa'
  download("https://qa.sageexchange.com/install/SageExchangeDesktopDistributer.msi","C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi")

end

And(/^user extracts SPS bootstrapper on local machine$/) do
    sleep 30
    ## If the download zip exists in Downloads folder
    if File.exist?("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip")
      Cucumber.logger.debug("File exists.")

      #Delete the old Bootstrapper file
      if File.exist?("C:\\Users\\#{$username}\\sed_installers\\#{@env}\\SageExchangeDesktopBootstrapper.exe")
        File.delete("C:\\Users\\#{$username}\\sed_installers\\#{@env}\\SageExchangeDesktopBootstrapper.exe")
      end
      #Extract current bootstrapper to the location
      extract_zip("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip","C:\\Users\\#{$username}\\sed_installers\\#{@env}\\")
    end
    expect(File.exist?("C:\\Users\\#{$username}\\sed_installers\\#{@env}\\SageExchangeDesktopBootstrapper.exe")).to be true
end

When (/^User Opens SageExchangeDesktopBootstrapper tool$/) do
    ##Start the Winium Driver
    shell = WIN32OLE.new('Shell.Application')
    #open Winium Desktop Driver - confirm the location
    shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
    # #Create new WiniumApp object
    installer_app = WiniumApp.new
    # #execute the launch_sage_installer_application method in class WiniumApp
    @install_app = installer_app.launch_sage_installer_application(@env)
end

When (/^User Opens SageExchangeDesktopDistributer.msi installer$/) do
  ##Start the Winium Driver
  shell = WIN32OLE.new('Shell.Application')
  #open Silent Installer
  shell.ShellExecute("C:\\Users\\#{$username}\\sed_installers\\#{@env}\\SageExchangeDesktopDistributer.msi")

  #wait for installation
  sleep 120
end

And (/^User launches Sage Exchange Desktop application$/) do
  ##Start the Winium Driver
  shell = WIN32OLE.new('Shell.Application')
  #open Sage Exchange Desktop
  shell.ShellExecute('C:\Program Files (x86)\Sage Payment Solutions\Sage Exchange Desktop\SageExchangeDesktop.spsdeployment')
end

And (/^User kills SED application$/) do
  system("taskkill /IM SageExchange.exe /f >nul 2>&1")
end

When(/^User stops SED application$/) do
  if find_process("SageExchange.exe") == '1'
    shell = WIN32OLE.new('Shell.Application')
    #open Winium Desktop Driver - confirm the location
    shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
    # #Create new WiniumApp object
    app = WiniumApp.new
    # #execute the launch_sage_exchange_desktop method in class WiniumApp
    # this launches the SED application
    sed_app = app.launch_desktop
    sleep 3
    sed_app.find_element(:id, "1502").click
    sleep 1
    sed = sed_app.find_element(:name, "Sage Exchange")
    sed_app.action.move_to(sed).perform
    sed_app.action.context_click(sed).perform
    sed_app.find_element(:name, "Exit").click

    system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
  end
end
#Below steps to uninstall SED if already installed
And (/^User uninstalls SED if already installed$/) do
    ## Verify is the SED is already installed by checking in C:\
    if File.exist? ("C:\\Program Files (x86)\\Sage Payment Solutions\\Sage Exchange Desktop\\SageExchangeDesktop.spsdeployment")
      window = @install_app.find_element(:class, "WixStdBA").find_element(:name, "Sage Exchange Desktop Setup")
      window.find_element(:name, "Uninstall").click
       sleep 45 #waiting for uninstall to be completed

      #Validate whether the text on the uninstall window is as expected
      assert(window.find_element(:name,"Operation completed successfully"))

      ## Close the window once uninstall is complete
      window.find_element(:class,"Button").find_element(:name, "Close").click
      ## Kill Bootstapper and Winium apps
      system("taskkill /IM SageExchangeDesktopBootstrapper.exe /f >nul 2>&1")
      system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
    end
end

#Below steps to uninstall SED if already installed
And (/^User uninstalls any version of SED$/) do
  ## Verify is the SED is already installed by checking in C:\
  window = @install_app.find_element(:class, "WixStdBA").find_element(:name, "Sage Exchange Desktop Setup")
  if File.exist? ("C:\\Program Files (x86)\\Sage Payment Solutions\\Sage Exchange Desktop\\SageExchangeDesktop.spsdeployment")
    puts window.find_elements(:name, "Uninstall").size
    if window.find_elements(:name, "Uninstall").size > 0
      window.find_element(:name, "Uninstall").click
      sleep 45 #waiting for uninstall to be completed
      #Validate whether the text on the uninstall window is as expected
      assert(window.find_element(:name,"Operation completed successfully"))
      ## Close the window once uninstall is complete
      window.find_element(:class,"Button").find_element(:name, "Close").click
      ## Kill Bootstapper and Winium apps
      system("taskkill /IM SageExchangeDesktopBootstrapper.exe /f >nul 2>&1")
      system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
      sleep 5
      # File.delete("C:\\ProgramData\\Sage Payment Solutions\\SageExchangeDesktop")
      # File.delete("C:\\Users\\#{$username}\\AppData\\Local\\Sage Payment Solutions")
    elsif window.find_elements(:name,"I agree to the license terms and conditions").size > 0
      window.find_element(:name, "Close").click
      window.find_element(:name, "Yes").click
      system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")

      @env = 'qa'
      # #execute the launch_sage_installer_application method in class WiniumApp
      ##Start the Winium Driver
      shell = WIN32OLE.new('Shell.Application')
      #open Winium Desktop Driver - confirm the location
      shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
      # #Create new WiniumApp object
      installer_app = WiniumApp.new
      # #execute the launch_sage_installer_application method in class WiniumApp
      @install_app = installer_app.launch_sage_installer_application(@env)
      window = @install_app.find_element(:class, "WixStdBA").find_element(:name, "Sage Exchange Desktop Setup")

      if window.find_elements(:name, "Uninstall").size > 0
        window.find_element(:name, "Uninstall").click
        sleep 60 #waiting for uninstall to be completed
        #Validate whether the text on the uninstall window is as expected
        assert(window.find_element(:name,"Operation completed successfully"))
        ## Close the window once uninstall is complete
        window.find_element(:class,"Button").find_element(:name, "Close").click
        ## Kill Bootstapper and Winium apps
        system("taskkill /IM SageExchangeDesktopBootstrapper.exe /f >nul 2>&1")
        system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
        sleep 5
        #File.delete("C:\\ProgramData\\Sage Payment Solutions\\SageExchangeDesktop")
        #File.delete("C:\\Users\\#{$username}\\AppData\\Local\\Sage Payment Solutions")
      end
    end
  else
    puts "SED is not available, please install"
    system("taskkill /IM SageExchangeDesktopBootstrapper.exe /f >nul 2>&1")
    system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
  end
end

And (/^User Installs SED in local machine$/) do
    window = @install_app.find_element(:class, "WixStdBA").find_element(:name, "Sage Exchange Desktop Setup")

    window.find_element(:name,"I agree to the license terms and conditions").click
    window.find_element(:name,"Install").click

    sleep 45 ## wait for SED to install
    # Close the window once install is done
    window.find_element(:class,"Button").find_element(:name, "Close").click
end

Then(/^user should be able see SED2.0 Logo on top right corner of installation window\.$/) do

  #window = @install_app.find_element(:class, "Window").find_element(:xpath, "//starts-with[@Name = 'Sage Payment Solutions Installer']//a")
  window = @install_app.find_element(:class, "Window").find_element(:name, "Sage Payment Solutions Installer v1.0.10.11025")
  ## Validate if image is available on the window
  assert(window.find_element(:class,"Image"))
end
# :xpath,"//div[@id = 'international-map']//a"
# *[starts-with(@Name, 'Element_')]

And(/^User clicks Download and Install button$/) do
    window = @install_app.find_element(:class, "Window").find_element(:name, "Sage Payment Solutions Installer v1.0.10.11025")
    ## Install the application for further tests
    window.find_element(:name,"Download and Install").click

    ## Wait to complete the installation
    sleep 120
    #Close the WiniumDriver
    system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
end

And (/^User changes hosts file to point production$/) do
  file_name = 'C:\Windows\System32\drivers\etc\hosts'
  text = File.read(file_name)
  unless text.include? "#10.18.115.50"
    new_contents = text.gsub(/10.18.115.50/, "#10.18.115.50")
    new_contents = new_contents.gsub(/10.18.115.52/, "#10.18.115.52")
    new_contents = new_contents.gsub(/10.18.115.53/, "#10.18.115.53")
    File.open(file_name, "w") {|file| file.puts new_contents }
  end
end

And (/^User changes hosts file to point QA$/) do
  file_name = 'C:\Windows\System32\drivers\etc\hosts'
  text = File.read(file_name)
  new_contents = text.gsub(/#10.18.115.50/, "10.18.115.50")
  new_contents = new_contents.gsub(/#10.18.115.52/, "10.18.115.52")
  new_contents = new_contents.gsub(/#10.18.115.53/, "10.18.115.53")

# To write changes to the file, use:
  File.open(file_name, "w") {|file| file.puts new_contents }
end

Given(/^User validates the current version of SED is (.*)$/) do |sed_version|
  ##Start the Winium Driver
  shell = WIN32OLE.new('Shell.Application')
  #open Winium Desktop Driver - confirm the location
  shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
  # #Create new WiniumApp object
  app = WiniumApp.new
  # #execute the launch_sage_exchange_desktop method in class WiniumApp
  # this launches the SED application
  @sed_app = app.launch_sage_exchange_desktop
  @version = get_sed_version(@sed_app)
  if @version.include? sed_version.to_s
    @sed_install = false
    Cucumber.logger.debug("SED Version is already installed ")
    puts "SED Version is already installed"
  else
    @sed_install = true
    Cucumber.logger.debug("SED Version needs to be installed ")
    puts false
  end

  #Close the WiniumDriver
  system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
  Cucumber.logger.debug("SED Version is #{@version}")

end

Given(/^User downloads SED (\d+)\.(\d+)\.(\d+)\.(\d+) from install page$/) do |arg1, arg2, arg3, arg4|
  pending # Write code here that turns the phrase above into concrete actions
end


And(/^User hovers over SED icon$/) do
  shell = WIN32OLE.new('Shell.Application')
  #open Winium Desktop Driver - confirm the location
  shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
  # #Create new WiniumApp object
  app = WiniumApp.new
  # #execute the launch_sage_exchange_desktop method in class WiniumApp
  # this launches the SED application
  sed_app = app.launch_desktop

  sed_app.find_element(:id, "1502").click
  button1 = sed_app.find_element(:id, "1502")
  sed_app.action.move_to(button1).perform
  sleep 1
  sed_app.find_element(:id, "1502").click
  sleep 1
  if sed_app.find_elements(:name, "Sage Exchange").size > 0
    sed = sed_app.find_element(:name, "Sage Exchange")
    sed_app.action.move_to(sed).perform
    sleep 2
  else
    puts "Sage Application not found"
  end
  system("taskkill /IM SpsDevboxCs.exe /f >nul 2>&1")
  system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
end

And(/^User adds required settings to SED$/) do
  shell = WIN32OLE.new('Shell.Application')
  #open Winium Desktop Driver - confirm the location
  shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
  # #Create new WiniumApp object
  app = WiniumApp.new
  # #execute the launch_sage_exchange_desktop method in class WiniumApp
  # this launches the SED application
  sed_app = app.launch_desktop
  sleep 5
  # el = sed_app.find_element(:id, "1502")
  # sed_app.action.move_to(el).perform
  # sleep 5
  sed_app.find_element(:id, "1502").click
  sleep 1
  # sed_app.find_element(:id, "1502").click
  # sleep 1
  # sed_app.find_element(:id, "1502").click
  # el2 = sed_app.find_element(:name, "NotificationOverflow")
  # sed_app.action.move_to(el2).perform
  # sleep 5
  # sed_app.find_element(:id, "1502").click
  # sed_app.find_element(:id, "1502").click
  sed = sed_app.find_element(:name, "Sage Exchange")
  sed_app.action.move_to(sed).perform
  sed_app.action.context_click(sed).perform
  sed_app.find_element(:name, "Settings").click
  window = sed_app.find_element(:id, "frmSettings")
  if window.find_element(:name, "Credit Card Devices").attribute("HasKeyboardFocus") == "true"
    puts "Credit Card Devices is selected by default"
  end

  window.find_element(:name, "Moneris iPP 320").click
  window.find_element(:name, "Moneris iPP 320").click
  window.find_element(:id, "ckbAutoStartSwipe").click
  window.find_element(:name, "Credit Card Devices").click
  window.find_element(:name,"OK").click
  window.find_element(:name,"OK").click
  system("taskkill /IM SpsDevboxCs.exe /f >nul 2>&1")
  system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")

end

When(/^User downloaded SED (\d+)\.(\d+)\.(\d+)\.(\d+) from install page$/) do |arg1, arg2, arg3, arg4|
pending # Write code here that turns the phrase above into concrete actions
end

And(/^User installs SED (\d+)\.(\d+)\.(\d+)\.(\d+)$/) do |arg1, arg2, arg3, arg4|
pending # Write code here that turns the phrase above into concrete actions
end

Then(/^User validates if settings are transfered$/) do
  shell = WIN32OLE.new('Shell.Application')
  #open Winium Desktop Driver - confirm the location
  shell.ShellExecute('C:\Winium.Desktop.Driver.exe')
  # #Create new WiniumApp object
  app = WiniumApp.new
  # #execute the launch_sage_exchange_desktop method in class WiniumApp
  # this launches the SED application
  sed_app = app.launch_desktop
  sed_app.find_element(:id, "1502").click
  sed = sed_app.find_element(:name, "Sage Exchange")
  sed_app.action.context_click(sed).perform
  sed_app.find_element(:name, "Settings").click
  window = sed_app.find_element(:id, "frmSettings")
  if window.find_element(:name, "Non-EMV Devices").attribute("HasKeyboardFocus") == "true"
    puts "Non EMV Devices is selected by default"
  end

  window.find_element(:name, "EMV Devices").click
  window.find_element(:name, "Moneris IPP320").click
  window.find_element(:name, "Moneris IPP320").click
  window.find_element(:name, "Moneris IPP320").click

  window.find_element(:id, "cbAutoStartSwipe").click
  window.find_element(:id, "cbAutoStartSwipe").click
  window.find_element(:name, "EMV Devices").click
  window.find_element(:name,"OK").click
  if window.find_elements(:name,"Sage Exchange will need to restart in order for your new settings to take effect.").size == 0
    puts "Settings saved from one version to another"
  elsif window.find_elements(:name,"Sage Exchange will need to restart in order for your new settings to take effect.").size > 0
    puts "Settings saved from one version to another"
  end
  # window.find_element(:name,"OK").click
end

Given(/^User downloads Device Driver Ingenico iPP(\d+)$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^User validates if the device driver is installed$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^User validates if Driver is installed and shows up in Control Panel$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

And(/^User validates the driver file is present at the required location$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^User installs SED (\d+)\.(\d+)$/) do |arg1, arg2|
  pending # Write code here that turns the phrase above into concrete actions
end

And(/^User Upgrades to SED (\d+)\.(\d+)$/) do |arg1, arg2|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^User Validates if Sage Payments Soultions folder is created$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

And(/^User Validates SageExchangeDesktop\.settings file$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^User should not be able to find anything in Program Data folder in AppData$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^User visits the SED install page$/) do
  download_directory = "#{Dir.pwd}/downloads"

  profile = Selenium::WebDriver::Firefox::Profile.new
  profile['browser.download.dir'] = download_directory
  profile['browser.helperApps.neverAsk.saveToDisk'] = 'text/csv,application/pdf,
                                                       application/zip,
                                                       application/octet-stream,
                                                       multipart/x-zip,
                                                       application/zip-compressed,
                                                       application/x-zip-compressed,
                                                       application/msword'
  @browser = Watir::Browser.new :chrome, profile: profile
  @browser.goto 'https://qa.sageexchange.com/install'
  @browser.driver.manage.window.maximize
end

When(/^User clicks Download tab for SED 1.x$/) do
  @browser.element(id: "templates").hover
  @browser.link(:text => "Sage Exchange Desktop v1.x").wait_until_present.click
  sleep(3)
end

When(/^User clicks Download tab for SED 2.x$/) do
  @browser.element(id: "templates").hover
  @browser.link(:text => "Sage Exchange Desktop v2.x").wait_until_present.click
  sleep(5)
  #until browser.element(:id=>"some_div").exists? do sleep 1 end
end

Then(/^User should able to validate the installers sections with required text$/) do
  expect(@browser.tds(:text => "Sage Exchange Desktop 1.0")[0]).to exist
  expect(@browser.tds(:text => "Sage Exchange Desktop 1.0")[1]).to exist
  expect(@browser.td(:text => "Module SDK")).to exist
  expect(@browser.td(:text => "Standard installer")).to exist
  expect(@browser.td(:text => "SED components, zipped")).to exist
  expect(@browser.td(:text => "Standalone .NET/COM components")).to exist
  expect(@browser.links(:text => "Download Installer")[0]).to exist
  expect(@browser.links(:text => "Download Installer")[1]).to exist
  expect(@browser.links(:text => "Download Installer")[2]).to exist
end

And(/^User clicks the Download installer link for (.*) installation$/) do |arg1|
  if arg1 == 'manual'
    download = @browser.links(:text => "Download Installer")[0]
    @browser.scroll.to :center
    download.click
    puts 'clicked download'
    #@browser.window(:index => 0).use
    agree = @browser.input(:type => "checkbox")
    agree.click
    #@browser.driver.action.move_to(agree.wd, 150, 10).click.perform
    puts 'Clicked agree.'
    sleep(3)
    @browser.link(:href => "SageExchangeDesktopBootstrapper.zip").click
    puts 'Downloading file.'
  else
    download = @browser.links(:text => "Download Installer")[1]
    @browser.scroll.to :center
    download.click
    puts 'clicked download'
    #@browser.window(:index => 0).use
    agree = @browser.input(:type => "checkbox")
    agree.click
    #@browser.driver.action.move_to(agree.wd, 150, 10).click.perform
    puts 'Clicked agree.'
    sleep(3)
    @browser.link(:href => "SageExchangeDesktopDistributer.msi").click
    puts 'Downloading file.'
  end
end

## Validate if the file has been downloaded and is present in Downloads folders
Then(/^User validates if (.*) installer is downloaded$/) do |arg1|
    sleep(30)
    @browser.close
    if arg1 == 'manual'
      expect(File.exist?("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopBootstrapper.zip")).to be true
      Cucumber.logger.debug("File exists.")
    else
      expect(File.exist?("C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi")).to be true
      Cucumber.logger.debug("File exists.")
      if @env == 'qa'
        system "copy C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi C:\\Users\\#{$username}\\sed_installers\\#{@env}\\"
      elsif @env == 'prod'
        system "copy C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi C:\\Users\\#{$username}\\sed_installers\\#{@env}\\"
      end
    end
end

Then(/^User validates the details on the Download Page$/) do
  expect(@browser.h3(:text => "Manual Installation")).to exist
  expect(@browser.td(:text => "Sage Exchange Desktop Distributor")).to exist
  expect(@browser.td(:text => "Standard installer")).to exist
  expect(@browser.links(:text => "Download Installer")[1]).to exist
end

After do
  #@browser.close
  #Close the application
  system("taskkill /IM SpsDevboxCs.exe /f >nul 2>&1")

  #Close the WiniumDriver
  system("taskkill /IM Winium.Desktop.Driver.exe /f >nul 2>&1")
end
