@transaction
  @ui
Feature: ACH Transaction 71173B

  Scenario: validating of default Account type
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And  User clicks Menu item TEMPLATES - TRANSACTION SALE UI and clicks GO
    Then SED pop up window Sage Exchange - Sale should open
    And User selects Payment type ACH
    And User clicks next
    And By default value of Account type dropdown should be 'Checking'