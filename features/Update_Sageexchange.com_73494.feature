@ui
Feature: Validate changes made to installers for SED 2.x

  Scenario: Validating updates to Download page
    Given User visits the SED install page
    When User clicks Download tab for SED 2.x
    Then User validates the details on the Download Page

  Scenario: Validating manual installation link
    Given User visits the SED install page
    When User clicks Download tab for SED 2.x
    And User clicks the Download installer link for manual installation
    Then User validates if the installer is downloaded

  Scenario: Validating silent installation link
    Given User visits the SED install page
    When User clicks Download tab for SED 2.x
    And User clicks the Download installer link for silent installation
    Then User validates if the installer is downloaded