
Feature:Alerts and Notifications
  Scenario: SED2.0.2.23 should be seen on the Task bar
  Install SED2.0 in windows 10
  Installing SED2.0.2.23 from install Page.
  1) User has to browse the Link
  https://qa.sageexchange.com/install/#/sedv2x/en.
  2) Under Manual Installation click Sage Exchange Desktop v2.x installer.
  3) Once download is done, open the folder and run the Sage Exchange Desktop Bootstrapper.
  4) Sage Exchange Desktop Setup pop up open. Select the check box and click Install tab and click yes.
  5)App deploy Pop up comes up and click on Download and Install tab.

  Scenario: On Taskbar, SED2.0 display the icon differently. It will have a pink colour border around it.
  Change the host files from QA to Dev
  1) On Taskbar, Right click SED2.0 and click Exit tab.
  2)From Start Menu  open Notepad right click and select "Run As Administrator".
  3)On notepad click File->open-C:\Windows\System32\drivers\etc.
  4) Comment QA host files and Uncomment Dev Host files and save it.
  5)From Start menu search for Sage Exchange Desktop and click.
  6)App Deploy Pop up open and click Skip tab.

  Scenario:Install Updates
  1) Click this menu and sed will get updated and also the new version of sed will launch.
  2) The icon should no longer have the pink border and the "Install Update" will not show in the menu.
  Testing  Install Updates for SED2.0
  1) On Taskbar, Right click SED2.0 the pop menu
  will have it very first item "Install Updates"
  with the windows sheild icon and it also shaded bule. Click Install update.
  2)Click yes.

