@transaction
@ui
Feature:   Transfer Setting SED2.0.2.16 to SED2.0.2.29

##Can only automate till validating transfer settings as we cannot automate card swipe

  Scenario: Ensure all settings related to Credit  Card devices from 2.0.2.16 transfer to EMV devices in 2.0.2.29 .
     #Download and Save QA Installer
     Given User changes hosts file to point QA
    # And User validates the current version of SED is 2.0.2.29
     And user downloads SED2.0 software from QA url
     And User validates if manual installer is downloaded
     And user extracts SPS bootstrapper on local machine

    #Download and Save Prod Installer
     And User changes hosts file to point production
     And user downloads SED2.0 software from Production url
     And User validates if manual installer is downloaded
     And user extracts SPS bootstrapper on local machine
     And User stops SED application
     # And User hovers over SED icon


    #Start the installer application to uninstall existing SED
     And User Opens SageExchangeDesktopBootstrapper tool
     And User uninstalls any version of SED
     # And User hovers over SED icon

    #Install Production SED
     When User changes hosts file to point production
     And user downloads SED2.0 software from Production url
     And User validates if manual installer is downloaded
     And user extracts SPS bootstrapper on local machine
     And User Opens SageExchangeDesktopBootstrapper tool
     And User Installs SED in local machine
     And User clicks Download and Install button
     # And User hovers over SED icon

    #Change Settings in 2.0.2.16

     And User adds required settings to SED

   #Install QA SED
     When User changes hosts file to point QA
     And user downloads SED2.0 software from QA url
     And User validates if manual installer is downloaded
     And user extracts SPS bootstrapper on local machine
     And User stops SED application
     And User Opens SageExchangeDesktopBootstrapper tool
     And User Installs SED in local machine
     And User clicks Download and Install button

     Then User validates if settings are transfered
