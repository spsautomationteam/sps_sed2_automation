## NOTE : this feature has to run via command line as admin


@installations
  @ui
Feature:  Validating SED2.0 Logo

  Scenario:  SED 2.0 logo validation
    Given  user downloads SED2.0 software from QA url
    And   User validates if manual installer is downloaded
    And  user extracts SPS bootstrapper on local machine
    When User Opens SageExchangeDesktopBootstrapper tool
    And User uninstalls SED if already installed
    And User Opens SageExchangeDesktopBootstrapper tool
    And User Installs SED in local machine
    Then  user should be able see SED2.0 Logo on top right corner of installation window.
    And User clicks Download and Install button
#
