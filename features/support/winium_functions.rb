require 'selenium-webdriver'
require 'rubygems'
require 'win32ole'

##username global variable is a ENVIRONMENT varaible from Ruby
$username = ENV['USERNAME']

#class which launches the windows app and connects Winium url to Selenium for Automation testing
class WiniumApp
  #Method for launching SPS DevBox along with Winium for automation testing
  def launch_sps_dev_application
    caps = Selenium::WebDriver::Remote::Capabilities.new
    #confirm the Location of application
    caps['app']  =  'C:\SPSDevBoxCs_latest_62416\SpsDevboxCs.exe'
    @driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999", desired_capabilities: caps
  end
  #Method for launching SED Installer along with Winium for automation testing
  def launch_sage_installer_application (env)
    caps = Selenium::WebDriver::Remote::Capabilities.new
    #confirm the Location of application
    puts env
    caps['app']  =  "C:\\Users\\#{$username}\\sed_installers\\#{env}\\SageExchangeDesktopBootstrapper.exe"
    @driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999", desired_capabilities: caps
  end
  #Method for launching SED Installer along with Winium for automation testing
  def launch_sage_silent_installer
    caps = Selenium::WebDriver::Remote::Capabilities.new
    #confirm the Location of application
    caps['app']  =  "C:\\Users\\#{$username}\\Downloads\\SageExchangeDesktopDistributer.msi"
    @driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999", desired_capabilities: caps
  end
  #Method for launching SED Installer along with Winium for automation testing
  def launch_sage_exchange_desktop
    caps = Selenium::WebDriver::Remote::Capabilities.new
    #confirm the Location of application
    caps['app']  =  'C:\Program Files (x86)\Sage Payment Solutions\Sage Exchange Desktop\SageExchangeDesktop.spsdeployment'
    @driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999" , desired_capabilities: caps
  end
  def launch_desktop
    caps = Selenium::WebDriver::Remote::Capabilities.new
    #confirm the Location of application
    caps['app']  =  'C:\SPSDevBoxCs_latest_62416\SpsDevboxCs.exe'
    @driver = Selenium::WebDriver.for :remote, url: "http://localhost:9999" , desired_capabilities: caps
  end

end
