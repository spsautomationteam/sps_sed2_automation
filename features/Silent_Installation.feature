@installations
Feature:Validation of Silent Installation upgrade version from SED2.0.2.16 to SED2.0.2.31 in Windows 10

  Scenario: SED2.0 should be Upgraded from SED2.0.2.16 to SED2.0.2.31( latest Version).
    #Download and Save QA Installer
    Given User changes hosts file to point QA
    And User validates the current version of SED is 2.0.2.16
    And user downloads SED2.0 software from QA url
    And User validates if manual installer is downloaded
    And user extracts SPS bootstrapper on local machine

    #Download and Save Prod Installer
    And User changes hosts file to point production
    And user downloads SED2.0 software from Production url
    And User validates if manual installer is downloaded
    And user extracts SPS bootstrapper on local machine

    #Start the installer application to uninstall existing SED
    And User kills SED application
    And User Opens SageExchangeDesktopBootstrapper tool
    And User uninstalls any version of SED


    #Install Production SED
    When User changes hosts file to point production
    And user downloads SED2.0 software from Production url
    And User validates if manual installer is downloaded
    And user extracts SPS bootstrapper on local machine
    And User Opens SageExchangeDesktopBootstrapper tool
    And User Installs SED in local machine
    And User clicks Download and Install button

    ##Downloads Silent Installer
    Then user downloads SED2.0 Silent Installer from QA url
    And User validates if silent installer is downloaded
    And User changes hosts file to point QA
    And User stops SED application
    And User Opens SageExchangeDesktopDistributer.msi installer
    And User launches Sage Exchange Desktop application
