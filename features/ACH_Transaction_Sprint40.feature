@transaction
@ui
Feature: ACH Transaction Sale and Auth for Class Type

  Scenario: Validating ACH Class type PPD - Auth
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And User provides XML input data for ach to SPSDevBox with valid MID
    And User clicks GO
    And SED pop up window Sage Exchange - Authorization should open
    When User selects Payment type ACH
    And User clicks next
    And User sets 123123123 for Routing Number in SED Payment Information
    And User sets 1234567890 for Account Number in SED Payment Information
    And User selects Checking for Account Type dropdown in SED Payment Information
    And User selects Prearranged Payment and Deposit Entry for Class dropdown in SED Payment Information
    And User clicks next
    And User clicks Submit button
    Then User should be able to validate the Response Message - ACCEPTED and Transaction Class - PPD


  Scenario: Validating ACH Class type CCD - Auth
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And User provides XML input data for ach to SPSDevBox with valid MID
    And User clicks GO
    And SED pop up window Sage Exchange - Authorization should open
    When User selects Payment type ACH
    And User clicks next
    And User sets 123123123 for Routing Number in SED Payment Information
    And User sets 1234567890 for Account Number in SED Payment Information
    And User selects Checking for Account Type dropdown in SED Payment Information
    And User selects Cash Concentration or Disbursement for Class dropdown in SED Payment Information
    And User clicks next
    And User clicks Submit button
    Then User should be able to validate the Response Message - ACCEPTED and Transaction Class - CCD


  Scenario: Validating ACH Class type CCD - Auth
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And User provides XML input data for ach to SPSDevBox with valid MID
    And User clicks GO
    And SED pop up window Sage Exchange - Authorization should open
    When User selects Payment type ACH
    And User clicks next
    And User sets 123123123 for Routing Number in SED Payment Information
    And User sets 1234567890 for Account Number in SED Payment Information
    And User selects Checking for Account Type dropdown in SED Payment Information
    And User selects Internet Initiated Entry for Class dropdown in SED Payment Information
    And User clicks next
    And User clicks Submit button
    Then User should be able to validate the Response Message - ACCEPTED and Transaction Class - WEB


  Scenario: Validating ACH Class type PPD - Sale
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And User provides XML input data for sale to SPSDevBox with valid MID
    And User clicks GO
    And SED pop up window Sage Exchange - Sale should open
    When User selects Payment type ACH
    And User clicks next
    And User sets 123123123 for Routing Number in SED Payment Information
    And User sets 1234567890 for Account Number in SED Payment Information
    And User selects Checking for Account Type dropdown in SED Payment Information
    And User selects Prearranged Payment and Deposit Entry for Class dropdown in SED Payment Information
    And User clicks next
    And User clicks Submit button
    Then User should be able to validate the Response Message - ACCEPTED and Transaction Class - PPD


  Scenario: Validating ACH Class type CCD - Sale
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And User provides XML input data for sale to SPSDevBox with valid MID
    And User clicks GO
    And SED pop up window Sage Exchange - Sale should open
    When User selects Payment type ACH
    And User clicks next
    And User sets 123123123 for Routing Number in SED Payment Information
    And User sets 1234567890 for Account Number in SED Payment Information
    And User selects Checking for Account Type dropdown in SED Payment Information
    And User selects Cash Concentration or Disbursement for Class dropdown in SED Payment Information
    And User clicks next
    And User clicks Submit button
    Then User should be able to validate the Response Message - ACCEPTED and Transaction Class - CCD


  Scenario: Validating ACH Class type CCD - Sale
    Given User Opens SPSDevBox tool
    And  User clicks SAGE EXCHANGE button in window SPS Integration Sandbox
    And User provides XML input data for sale to SPSDevBox with valid MID
    And User clicks GO
    And SED pop up window Sage Exchange - Sale should open
    When User selects Payment type ACH
    And User clicks next
    And User sets 123123123 for Routing Number in SED Payment Information
    And User sets 1234567890 for Account Number in SED Payment Information
    And User selects Checking for Account Type dropdown in SED Payment Information
    And User selects Internet Initiated Entry for Class dropdown in SED Payment Information
    And User clicks next
    And User clicks Submit button
    Then User should be able to validate the Response Message - ACCEPTED and Transaction Class - WEB