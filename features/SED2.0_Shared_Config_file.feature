@installations
@ui
Feature:  Validating Shared Config file settings

  Scenario:  Validate Shared config file
    Given User installs SED 1.0
    And User Upgrades to SED 2.0
    When User Validates if Sage Payments Soultions folder is created
    And User Validates SageExchangeDesktop.settings file
    Then User should not be able to find anything in Program Data folder in AppData