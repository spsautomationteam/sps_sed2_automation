@ui
Feature: Validate changes made to installers to SED 1.x

  Scenario: Testing updates to Installers
    Given User visits the SED install page
    When User clicks Download tab for SED 1.x
    Then User should able to validate the installers sections with required text